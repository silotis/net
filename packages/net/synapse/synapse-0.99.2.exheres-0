# Copyright 2018-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=matrix-org tag=v${PV} ] \
    setup-py [ import=setuptools has_bin=true multibuild=false test=pytest ] \
    systemd-service

SUMMARY="Matrix reference homeserver"
DESCRIPTION="
Synapse is the reference python/twisted Matrix homeserver implementation.
"
HOMEPAGE+=" https://matrix.org"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# Missing test dependencies
RESTRICT="test"

# See synapse/python_dependencies.py
DEPENDENCIES="
    build+run:
        group/${PN}
        user/${PN}
        dev-python/attrs[>=17.4.0][python_abis:*(-)?]
        dev-python/bcrypt[>=3.1.0][python_abis:*(-)?]
        dev-python/canonicaljson[>=1.1.3][python_abis:*(-)?]
        dev-python/daemonize[>=2.3.1][python_abis:*(-)?]
        dev-python/frozendict[>=1][python_abis:*(-)?]
        dev-python/jsonschema[>=2.5.1][python_abis:*(-)?]
        dev-python/msgpack[>=0.5.0][python_abis:*(-)?]
        dev-python/netaddr[>=0.7.18][python_abis:*(-)?]
        dev-python/phonenumbers[>=8.2.0][python_abis:*(-)?]
        dev-python/Pillow[>=3.1.2][python_abis:*(-)?]
        dev-python/prometheus_client[>=0.0.18&<0.4.0][python_abis:*(-)?]
        dev-python/psutil[>=2.0.0][python_abis:*(-)?]
        dev-python/pyasn1[>=0.1.9][python_abis:*(-)?]
        dev-python/pyasn1-modules[>=0.0.7][python_abis:*(-)?]
        dev-python/pymacaroons[>=0.13.0][python_abis:*(-)?]
        dev-python/pynacl[>=1.2.1][python_abis:*(-)?]
        dev-python/pyopenssl[>=16.0.0][python_abis:*(-)?]
        dev-python/PyYAML[>=3.11][python_abis:*(-)?]
        dev-python/service_identity[>=16.0.0][python_abis:*(-)?]
        dev-python/signedjson[>=1.0.0][python_abis:*(-)?]
        dev-python/six[>=1.10][python_abis:*(-)?]
        dev-python/sortedcontainers[>=1.4.4][python_abis:*(-)?]
        dev-python/treq[>=15.1][python_abis:*(-)?]
        dev-python/unpaddedbase64[>=1.1.0][python_abis:*(-)?]
        net-twisted/Twisted[>=18.7.0][python_abis:*(-)?]
    suggestion:
        dev-python/Jinja2[>=2.9][python_abis:*(-)?] [[
            description = [ Required for consent tracking and mail notifications ]
        ]]
        dev-python/bleach[>=1.4.2][python_abis:*(-)?] [[
            description = [ Required for mail notifications ]
        ]]
        dev-python/lxml[>=3.5.0][python_abis:*(-)?] [[
            description = [ Required for URL preview support ]
        ]]
        dev-python/matrix-synapse-ldap3[>=0.1][python_abis:*(-)?] [[
            description = [ Adds LDAP authentification support to Synapse ]
        ]]
        dev-python/psycopg2[>=2.6][python_abis:*(-)?] [[
            description = [ Required for PostgreSQL support ]
        ]]
        dev-python/pysaml2[>=3.0.0][python_abis:*(-)?] [[
            description = [ Required for SAML 2.0 single sign-on support ]
        ]]
        dev-python/txacme[>=0.9.2][python_abis:*(-)?] [[
            description = [ Required to provision TLS certificates from e.g. Let's Encrypt ]
        ]]
"

src_install() {
    setup-py_src_install

    keepdir /var/lib/synapse
    edo chown synapse:synapse "${IMAGE}"/var/lib/synapse

    install_systemd_files

    # works with Python3
    edo sed \
        -e "s:/usr/bin/python2.7:${PYTHON}:" \
        -i "${IMAGE}"/${SYSTEMDSYSTEMUNITDIR}/${PN}.service
}

pkg_postinst() {
    elog "Generate a default configuration file:"
    elog "cd /var/lib/synapse"
    elog "${PYTHON} -m synapse.app.homeserver \\"
    elog "    --server-name my.domain.name \\"
    elog "    --config-path /etc/synapse/homeserver.yaml \\"
    elog "    --generate-config \\"
    elog "    --report-stats=no"
}

